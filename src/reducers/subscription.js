import { createSlice } from '@reduxjs/toolkit';
export const subscriptionSlice = createSlice({
  name: 'subscription',
  initialState: {
    subscribed: false,
  },
  reducers: {
    toggleSubscription: (state) => {
      state.subscribed = !state.subscribed;
    },
    setSubscription: (state, action) => {
      state.subscribed = action.payload;
    },
  },
});

export const { toggleSubscription, setSubscription } = subscriptionSlice.actions;

export const fetchSubscribe = (email) => async (dispatch) => {
  try {
    const response = await fetch('http://localhost:3000/subscribe', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ email }),
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return { error: 'Something went wrong' };
  }
};

export const fetchUnsubscribe = () => async (dispatch) => {
  try {
    const response = await fetch('http://localhost:3000/unsubscribe', { method: 'POST' });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
    return { error: 'Something went wrong' };
  }
};

export default subscriptionSlice.reducer;
