import { createSlice } from "@reduxjs/toolkit";

const sectionSlice = createSlice({
  name: "section",
  initialState: {
    communityRender: true,
  },
  reducers: {
    toggleCommunity: (state) => {
      state.communityRender = !state.communityRender;
    },
  },
});

export const { toggleCommunity } = sectionSlice.actions;

export default sectionSlice.reducer;
