
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import { subscriptionSlice, fetchSubscribe, fetchUnsubscribe } from "../reducers/subscription";

const mockStore = configureStore([thunk]);

describe("subscriptionSlice", () => {
  it("should toggle subscription status", () => {
    const initialState = { subscribed: false };
    const store = mockStore({ subscription: initialState });

    store.dispatch(subscriptionSlice.actions.toggleSubscription());

    const actions = store.getActions();
    const expectedPayload = { type: "subscription/toggleSubscription", payload: undefined };
    expect(actions).toEqual([expectedPayload]);
  });

  it("should set subscription status", () => {
    const initialState = { subscribed: false };
    const store = mockStore({ subscription: initialState });

    const subscribed = true;
    store.dispatch(subscriptionSlice.actions.setSubscription(subscribed));

    const actions = store.getActions();
    const expectedPayload = { type: "subscription/setSubscription", payload: subscribed };
    expect(actions).toEqual([expectedPayload]);
  });
});

describe("fetchSubscribe", () => {
  it("should fetch subscribe data successfully", async () => {
    const email = "test@test.com";
    const response = { status: "success" };
    global.fetch = jest.fn().mockResolvedValueOnce({
      json: () => Promise.resolve(response),
    });

    const dispatch = jest.fn();
    const result = await fetchSubscribe(email)(dispatch);

    expect(fetch).toHaveBeenCalledWith("http://localhost:3000/subscribe", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email }),
    });

    expect(result).toEqual(response);
  });

  it("should handle fetch subscribe error", async () => {
    const email = "test@test.com";
    const error = "Something went wrong";
    global.fetch = jest.fn().mockRejectedValueOnce(error);

    const dispatch = jest.fn();
    const result = await fetchSubscribe(email)(dispatch);

    expect(fetch).toHaveBeenCalledWith("http://localhost:3000/subscribe", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email }),
    });

    expect(result).toEqual({ error });
  });
});

describe("fetchUnsubscribe", () => {
  it("should fetch unsubscribe data successfully", async () => {
    const response = { status: "success" };
    global.fetch = jest.fn().mockResolvedValueOnce({
      json: () => Promise.resolve(response),
    });

    const dispatch = jest.fn();
    const result = await fetchUnsubscribe()(dispatch);

    expect(fetch).toHaveBeenCalledWith("http://localhost:3000/unsubscribe", { method: "POST" });
    expect(result).toEqual(response);
  });

  it("should handle fetch unsubscribe error", async () => {
    const error = "Something went wrong";
    global.fetch = jest.fn().mockRejectedValueOnce(error);

    const dispatch = jest.fn();
    const result = await fetchUnsubscribe()(dispatch);

    expect(fetch).toHaveBeenCalledWith("http://localhost:3000/unsubscribe", { method: "POST" });
    expect(result).toEqual({ error });
  });
});
