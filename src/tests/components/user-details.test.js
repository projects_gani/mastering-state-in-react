import { render, screen, waitFor } from "@testing-library/react";
import { MemoryRouter, Route, Routes } from "react-router-dom";
import UserDetails from "../../components/user-details/user-details";
import "@testing-library/jest-dom/extend-expect";

jest.mock("node-fetch");

describe("UserDetails", () => {
  test("renders user details", async () => {
    const userData = {
      id: "1",
      firstName: "Gary",
      lastName: "Newman",
      position: "Lead Designer at Company Name",
      avatar: "http://localhost:3000/avatars/avatar3.png",
    };

    const response = {
      ok: true,
      json: jest.fn().mockResolvedValue(userData),
    };

    global.fetch = jest.fn().mockResolvedValue(response);

    render(
        <MemoryRouter initialEntries={[`/user/${userData.id}`]}>
            <Routes>
                <Route path="/user/:id" element={<UserDetails />}/>
            </Routes>
        </MemoryRouter>
    );

    await waitFor(() => {
      expect(fetch).toHaveBeenCalledTimes(1);
    });
    await waitFor(() => {
        expect(fetch).toHaveBeenCalledWith(
          `http://localhost:3000/community/${userData.id}`
        );
      });

    //expect(screen.getByAltText("user avatar")).toHaveAttribute("src", userData.avatar);
    // expect(screen.getByText(`${userData.firstName} ${userData.lastName}`)).toBeInTheDocument();
    // expect(screen.getByText(userData.position)).toBeInTheDocument();
  });

  test("renders NotFound component on fetch error", async () => {
    const error = new Error("Failed to fetch user details");

    const response = {
      ok: false,
      statusText: error.message,
    };

    global.fetch = jest.fn().mockResolvedValue(response);

    render(
        <MemoryRouter initialEntries={["/user/1"]}>
            <Routes>
                <Route path="/user/:id" element={<UserDetails />}/>
            </Routes>
        </MemoryRouter>
    );

    await waitFor(() => {
      expect(fetch).toHaveBeenCalledTimes(1);
    });

    await waitFor(() => {
      expect(fetch).toHaveBeenCalledWith("http://localhost:3000/community/1");
    });

    await waitFor(() => {
      expect(screen.getByText(/Page Not found/i)).toBeInTheDocument();
    });
    
  });
});
