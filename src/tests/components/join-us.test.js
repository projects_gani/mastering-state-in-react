import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import '@testing-library/jest-dom';
import JoinUs from '../../components/join-us/join-us';
import store from '../../store';
import { setSubscription } from '../../reducers/subscription';
import {expect} from '@jest/globals';

const server = setupServer(
  rest.post('http://localhost:3000/subscribe', (req, res, ctx) => {
    return res(ctx.json({ success: true }));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('JoinUs component', () => {

  it('subscribes the user when Subscribe button is clicked', async () => {
    render(
      <Provider store={store}>
        <JoinUs />
      </Provider>
    );
  
    const emailInput = screen.getByPlaceholderText('Enter your email');
    const subscribeButton = screen.getByText('Subscribe');
  
    fireEvent.change(emailInput, { target: { value: 'test@example.com' } });
    fireEvent.click(subscribeButton);
    expect(subscribeButton.disabled).toBe(true);
  
    const unsubscribeButton = screen.getByText('Unsubscribe');
    expect(unsubscribeButton).toBeInTheDocument();
  });

  it('unsubscribes the user when Unubscribe button is clicked', async () => {
    render(
      <Provider store={store}>
        <JoinUs />
      </Provider>
    );

    const unsubscribeButton = screen.getByText('Unsubscribe');

    fireEvent.click(unsubscribeButton);
    expect(unsubscribeButton.disabled).toBe(true);
    const subscribeButton = screen.getByText('Subscribe');
    expect(subscribeButton).toBeInTheDocument();
  });

  it('should display the unsubscribe button and hide the subscribe button and input when subscribed', async () => {
    render(
      <Provider store={store}>
        <JoinUs />
      </Provider>
    );

    expect(screen.getByRole('button', { name: 'Subscribe' })).toBeVisible();
    expect(screen.getByRole('textbox', { name: '' })).toBeVisible();
    expect(screen.queryByRole('button', { name: 'Unsubscribe' })).not.toBeInTheDocument();

    store.dispatch(setSubscription(true));

    
    await waitFor(() => expect(screen.queryByRole('button', { name: 'Subscribe' })).not.toBeInTheDocument());
    expect(screen.queryByRole('textbox', { name: '' })).not.toBeInTheDocument();
    expect(screen.getByRole('button', { name: 'Unsubscribe' })).toBeVisible();
  });

  it('should display the subscribe button and input and hide the unsubscribe button when unsubscribed', async () => {
    render(
      <Provider store={store}>
        <JoinUs />
      </Provider>
    );
  
    expect(screen.queryByRole('button', { name: 'Subscribe' })).not.toBeInTheDocument();
    expect(screen.queryByRole('textbox', { name: '' })).not.toBeInTheDocument();
    expect(screen.getByRole('button', { name: 'Unsubscribe' })).toBeVisible();
  
    store.dispatch(setSubscription(false));
  
    await waitFor(()=> expect(screen.getByRole('button', { name: 'Subscribe' })).toBeVisible());
    await waitFor(()=>expect(screen.getByRole('textbox', { name: '' })).toBeVisible());
    await waitFor(()=>expect(screen.queryByRole('button', { name: 'Unsubscribe' })).not.toBeInTheDocument()); 
  });
  
  it('should enable the subscribe button after API call is finished', async () => {
    render(
      <Provider store={store}>
        <JoinUs />
      </Provider>
    );
  
    const emailInput = screen.getByPlaceholderText('Enter your email');
    const subscribeButton = screen.getByText('Subscribe');
  
    fireEvent.change(emailInput, { target: { value: 'test@example.com' } });
    fireEvent.click(subscribeButton);
  
    // Wait for the API call to finish
    await waitFor(() => expect(screen.queryByRole('button', { name: 'Unsubscribe' })));
  
    // Check that the button is enabled and has an opacity of 1
    await waitFor(()=> expect(subscribeButton.disabled).toBe(false)); 
    await waitFor(()=>expect(subscribeButton).not.toHaveStyle('opacity: 0.5'));
  });
  
  
});
