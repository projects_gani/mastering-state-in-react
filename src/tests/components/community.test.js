import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import Community from "../../components/community/community";
import { toggleCommunity } from "../../reducers/section";
import { BrowserRouter } from "react-router-dom";
import axios from 'axios'
import MockAdapter from "axios-mock-adapter";

const mockStore = configureStore([]);

const usersData = [
    {
        "id": "2f1b6bf3-f23c-47e4-88f2-e4ce89409376",
        "avatar": "http://localhost:3000/avatars/avatar1.png",
        "firstName": "Mary",
        "lastName": "Smith",
        "position": "Lead Designer at Company Name"
        },
        {
        "id": "1157fea1-8b72-4a9e-b253-c65fa1556e26",
        "avatar": "http://localhost:3000/avatars/avatar2.png",
        "firstName": "Bill",
        "lastName": "Filler",
        "position": "Lead Engineer at Company Name"
        },
        {
        "id": "b96ac290-543c-4403-80fe-0c2d44e84ea9",
        "avatar": "http://localhost:3000/avatars/avatar3.png",
        "firstName": "Tim",
        "lastName": "Gates",
        "position": "CEO at Company Name"
        },
        {
        "id": "94bf5157-1304-4eb8-9d12-fb292e14a041",
        "avatar": "http://localhost:3000/avatars/avatar3.png",
        "firstName": "Gary",
        "lastName": "Newman",
        "position": "Lead Designer at Company Name"
        },
        {
        "id": "da5c574b-c5c0-41b6-a3f1-15b8830d6397",
        "avatar": "http://localhost:3000/avatars/avatar2.png",
        "firstName": "Mike",
        "lastName": "Ivanov",
        "position": "Lead Engineer at Company Name"
        },
        {
        "id": "d71d13c2-e23f-4452-a5a3-0a3840f47c08",
        "avatar": "http://localhost:3000/avatars/avatar1.png",
        "firstName": "Tim",
        "lastName": "Gates",
        "position": "CEO at Company Name"
        },
        {
        "id": "d580c3ac-d80c-4f76-a8b2-efaacab9a753",
        "avatar": "http://localhost:3000/avatars/avatar2.png",
        "firstName": "Fill",
        "lastName": "Johns",
        "position": "Lead Designer at Company Name"
        },
        {
        "id": "04205477-355b-427e-9f90-98a922dcb0bd",
        "avatar": "http://localhost:3000/avatars/avatar3.png",
        "firstName": "Jameson",
        "lastName": "Brown",
        "position": "Lead Engineer at Company Name"
        },
        {
        "id": "94bf5157-1304-4eb8-9d12-fb292e14a041",
        "avatar": "http://localhost:3000/avatars/avatar1.png",
        "firstName": "Kim",
        "lastName": "Chang",
        "position": "CEO at Company Name"
        }
];

const initialState = {
  users: usersData,
  section: {
    communityRender: true,
  },
};

describe("Community component", () => {
  let mockAxios;

  beforeEach(() => {
    mockAxios = new MockAdapter(axios);
  });

  afterEach(() => {
    mockAxios.reset();
  });

  test("renders the section title", () => {
    const store = mockStore(initialState);
    render(
      <Provider store={store}>
        <BrowserRouter>
          <Community />
        </BrowserRouter>
      </Provider>
    );
    const titleElement = screen.getByText(/big community of/i);
    expect(titleElement).toBeInTheDocument();
  });

  test("renders the user feedback items", () => {
    const store = mockStore(initialState);
    render(
      <Provider store={store}>
        <BrowserRouter>
          <Community />
        </BrowserRouter>
      </Provider>
    );
    const userFeedbackElements = screen.getByText(/Gary/i);
    expect(userFeedbackElements).toBeInTheDocument();
  });

  test("dispatches toggleCommunity action on button click", () => {
    const store = mockStore(initialState);
    render(
      <Provider store={store}>
        <BrowserRouter>
          <Community />
        </BrowserRouter>
      </Provider>
    );
    const buttonElement = screen.getByRole("button", { name: /hide section/i });
    buttonElement.click();
    const actions = store.getActions();
    expect(actions).toContainEqual(toggleCommunity());
  });

  test("handles fetch error", async () => {
    const store = mockStore(initialState);
    mockAxios.onGet("/api/users").reply(500, "Failed to fetch community data");
    const consoleSpy = jest.spyOn(console, "log");

    render(
      <Provider store={store}>
        <BrowserRouter>
          <Community />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(() => expect(consoleSpy).toHaveBeenCalledWith("Failed to fetch community data: TypeError: Network request failed"));

    consoleSpy.mockRestore();
  });

//   test("dispatches setUsers action on mount", async () => {
//     const store = mockStore(initialState);
//     mockAxios.onGet("/api/users").reply(200, usersData);

//     render(
//       <Provider store={store}>
//         <BrowserRouter>
//           <Community />
//         </BrowserRouter>
//       </Provider>
//     );

//     await screen.findByText(/Kim Chang/i);

//     const actions = store.getActions();
//     expect(actions).toContainEqual(setUsers(usersData));
//   });
});
  

