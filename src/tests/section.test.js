import sectionReducer, { toggleCommunity } from '../reducers/section';
import { createStore } from 'redux'


test('toggleCommunity should toggle the communityRender state', () => {
    const initialState = {
      communityRender: true,
    };
    const store = createStore(sectionReducer, initialState);
    expect(store.getState().communityRender).toBe(true);
  
    store.dispatch(toggleCommunity());
    expect(store.getState().communityRender).toBe(false);
  
    store.dispatch(toggleCommunity());
    expect(store.getState().communityRender).toBe(true);
  });
  