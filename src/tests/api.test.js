import { api } from "../middleware/api";

test('api middleware dispatches correct actions for each suffix', () => {
    const mockDispatch = jest.fn();
    const next = jest.fn();
    const actionPending = { type: 'EXAMPLE_ACTION/pending', meta: { arg: 'test payload' } };
    const actionFulfilled = { type: 'EXAMPLE_ACTION/fulfilled', payload: 'test response' };
    const actionRejected = { type: 'EXAMPLE_ACTION/rejected', error: { message: 'test error' } };
  
    api({ dispatch: mockDispatch })(next)(actionPending);
    expect(mockDispatch).toHaveBeenCalledWith({ type: 'EXAMPLE_ACTION', payload: 'test payload' });
  
    api({ dispatch: mockDispatch })(next)(actionFulfilled);
    expect(mockDispatch).toHaveBeenCalledWith({ type: 'EXAMPLE_ACTION', payload: 'test response' });
  
    api({ dispatch: mockDispatch })(next)(actionRejected);
    expect(mockDispatch).toHaveBeenCalledWith({ type: 'EXAMPLE_ACTION', error: 'test error' });
  });
  