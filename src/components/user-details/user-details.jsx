import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import NotFound from "../not-found/not-found";

const UserDetails = () => {
  const { id } = useParams();
  const [userData, setUserData] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch(`http://localhost:3000/community/${id}`)
      .then((response) => response.json())
      .then((data) => {
        setUserData(data);
      })
      .catch((error) => {
        console.log(`Failed to fetch user details: ${error}`);
        setError(error);
      });
  }, [id]);

  if (error) {
    return <NotFound data-testid="not-found" />
  }

  return (
    <div className="app-feedback__item">
      {userData && (
        <>
          <img
            src={userData.avatar}
            alt=""
            className="app-feedback__item--img"
          />
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolor.
          </p>
          <h4 className="app-feedback__userName">
            {userData.firstName} {userData.lastName}
          </h4>
          <p>{userData.position}</p>
        </>
      )}
    </div>
  );
};

export default UserDetails;
