import { useEffect, useState, useRef } from 'react';
import '../join-us/join-us.css';
import { useDispatch, useSelector } from 'react-redux';
import { setSubscription, fetchSubscribe, fetchUnsubscribe } from '../../reducers/subscription';

const JoinUs = () => {
  const [email, setEmail] = useState('');
  const inputRef = useRef(null);
  const subscribeBtnRef = useRef(null);
  const unsubscribeBtnRef = useRef(null);
  const dispatch = useDispatch();
  const isSubscribed = useSelector((state) => state.subscription.subscribed);

  const renderForm = () => {
    if (isSubscribed) {
      inputRef.current.style.display = 'none';
      subscribeBtnRef.current.style.display = 'none';
      unsubscribeBtnRef.current.style.display = 'block';
    } else {
      inputRef.current.value = '';
      inputRef.current.style.display = 'block';
      subscribeBtnRef.current.style.display = 'block';
      unsubscribeBtnRef.current.style.display = 'none';
    }
  };

  const disableButton = (buttonRef) => {
    buttonRef.current.style.opacity = '0.5';
    buttonRef.current.disabled = true;
  };

  const enableButton = (buttonRef) => {
    if (buttonRef.current) {
      buttonRef.current.style.opacity = '1';
      buttonRef.current.disabled = false;
    }
  };

  const handleChange = (e) => {
    setEmail(e.target.value);
  };

  const handleSubscribe = async () => {
    disableButton(subscribeBtnRef);
    try {
      const data = await dispatch(fetchSubscribe(email));
      if (data.success) {
        dispatch(setSubscription(true));
        localStorage.setItem('subscribed', true);
        renderForm();
      } else if (data.error) {
        alert(data.error);
      }
    } catch (error) {
      console.error(error);
      alert(error.message);
    } 
    enableButton(subscribeBtnRef);
  };

  const handleUnsubscribe = async () => {
    disableButton(unsubscribeBtnRef);
    try {
      const data = await dispatch(fetchUnsubscribe());
      if (data.success) {
        dispatch(setSubscription(false));
        localStorage.setItem('subscribed', false);
        localStorage.removeItem('email');
        renderForm();
      }
    } catch (error) {
      console.error(error);
      alert(error.message);
    } 
    enableButton(unsubscribeBtnRef);

  };

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  useEffect(() => {
    renderForm();
  }, [isSubscribed]);


  return (
    <section className="app-section app-section--image-program">
      <h2 className="app-title">Join Our Program</h2>
      <h3 className="app-subtitle">
        Sed do eiusmod tempor incididunt <br /> ut labore et dolore magna
        aliqua.
      </h3>
      <form className="data" onSubmit={handleSubmit}>
          <input
            type="email"
            name="email"
            placeholder="Enter your email"
            onChange={handleChange}
            value={email}
            ref={inputRef}
            className="app-section__email"
          />
          <button
            className="app-section__button app-section__button--subscribe"
            type="submit"
            name="subscribe"
            onClick={handleSubscribe}
            ref={subscribeBtnRef}
          >
            Subscribe
          </button>
          <button
            className="app-section__button app-section__button--unsubscribe"
            type="submit"
            name="unsubscribe"
            onClick={handleUnsubscribe}
            ref={unsubscribeBtnRef}
          >
            Unsubscribe
          </button>
      </form>
    </section>
  );
};
export default JoinUs;
