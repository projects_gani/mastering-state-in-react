import { useSelector } from "react-redux";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { setUsers } from "../../reducers/users";
import { toggleCommunity } from "../../reducers/section";
import { Link } from "react-router-dom";
import "../community/community.css";

const Community = () => {
  const communityData = useSelector((state) => state.users);
  const communityRender = useSelector((state) => state.section.communityRender);

  const dispatch = useDispatch();

  const handleToggleSection = () => {
    dispatch(toggleCommunity());
  };

  useEffect(() => {
    fetch("http://localhost:3000/community")
      .then((response) => response.json())
      .then((data) => {
        dispatch(setUsers(data));
      })
      .catch((error) => {
        console.log("Failed to fetch community data: " + error);
        dispatch(setUsers([]));
      });
  }, [dispatch]);

  return (
    <div className="section-wrapper">
      <h2 className="app-title">
        Big Community of <br /> People Like You
      </h2>
      <button className="section-btn" name="show section" onClick={handleToggleSection}>
        {communityRender ? "Hide Section" : "Show Section"}
      </button>
      {communityRender && (
        <section className="app-section app-section--feedback">
          <h3 className="app-subtitle">
            We're proud of our products, and we're really excited <br /> when we
            get feedback from our users.
          </h3>
          <div className="app-feedback">
            {communityData &&
              communityData.map((item, i) => {
                return (
                  <div className="app-feedback__item" name="app-feedback__item" key={i}>
                    <img
                      src={item.avatar}
                      alt="user avatar"
                      className="app-feedback__item--img"
                    />
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolor.
                    </p>
                    <Link to={`/community/${item.id}`}>
                      <h4 className="app-feedback__userName">
                        {item.firstName} {item.lastName}
                      </h4>
                    </Link>
                    <p>{item.position}</p>
                  </div>
                );
              })}
          </div>
        </section>
      )}
    </div>
  );
};

export default Community;
