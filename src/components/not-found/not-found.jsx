import { Link } from "react-router-dom";
import './not-found.css';

const NotFound = () => {
    return (
      <div id="not-found" className="not-found">
        <h2>Page Not found</h2>
        <p>Looks like you've followed a broken link or entered a URL that doesn't exist on this site.</p>
        <Link to={`/`} className="not-found__link"><span>&#8592;</span>Back to our site</Link>
      </div>
    );
  };
  
  export default NotFound;