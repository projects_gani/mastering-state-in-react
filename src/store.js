import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import usersReducer from './reducers/users';
import sectionReducer from './reducers/section';
import subscriptionReducer from './reducers/subscription';
import { api } from './middleware/api';

export default configureStore({
  reducer: {
    users: usersReducer,
    section: sectionReducer,
    subscription: subscriptionReducer,
  },
  middleware: [...getDefaultMiddleware(), api],
});
