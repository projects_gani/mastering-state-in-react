export const api = ({ dispatch }) => next => action => {
    if (action.type.endsWith('/pending')) {
      // Dispatch the pending action
      dispatch({ type: action.type.replace('/pending', ''), payload: action.meta.arg });
    } else if (action.type.endsWith('/fulfilled')) {
      // Dispatch the fulfilled action with the response data
      dispatch({ type: action.type.replace('/fulfilled', ''), payload: action.payload });
    } else if (action.type.endsWith('/rejected')) {
      // Dispatch the rejected action with the error message
      dispatch({ type: action.type.replace('/rejected', ''), error: action.error.message });
    }
  
    // Pass the action to the next middleware in the chain
    return next(action);
  };
  