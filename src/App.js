import "./App.css";
import Community from "./components/community/community";
import JoinUs from "./components/join-us/join-us";


function App() {
  return (
    <div className="section-wrapper">
      <Community/>
      <JoinUs/>
    </div>
  );
}

export default App;
